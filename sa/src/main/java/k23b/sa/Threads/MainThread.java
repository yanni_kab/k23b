package k23b.sa.Threads;

import java.util.ArrayList;
import java.util.List;

import k23b.sa.Job;
import k23b.sa.Result;
import k23b.sa.Settings;
import k23b.sa.BlockingQueue.IBlockingQueue;
import k23b.sa.BlockingQueue.WaitNotifyQueue;
import k23b.sa.ExecutorService.IExecutorService;
import k23b.sa.ExecutorService.ThreadPool;
import k23b.sa.JobProvider.IJobProvider;
import k23b.sa.JobProvider.JobGenerator;
import k23b.sa.JobProvider.JobReader;
import k23b.sa.JobProvider.LineParser;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * The main thread that provides the functionality of the Software Agent
 *
 */
public class MainThread extends Thread {

	private static final Logger log = Logger.getLogger(MainThread.class);

	public MainThread() {

		setName("Main");
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public void run() {

		// log4j configuration
		PropertyConfigurator.configure("log4j.properties");

		log.info("");
		log.info("Starting Software Agent.");
		log.info("Working directory: " + System.getProperty("user.dir"));

		// load settings
		Settings settings = new Settings("sa.properties");
		settings.load();

		// configure line parser to run nmap with sudo if needed
		LineParser.setRunNmapAsRoot(settings.getRunNmapAsRoot());

		// periodic threads list
		List<Thread> periodicThreadList = new ArrayList<Thread>();

		// thread pool
		IExecutorService executorService = new ThreadPool("threadPool", settings.getThreadPoolSize());

		// The shared queue of the job results
		IBlockingQueue<Result> resultsBlockingQueue = new WaitNotifyQueue<Result>("resultsBlockingQueue", settings.getResultsQueueMax());

		// Start the SenderThread, begin to periodically send results to AM
		Thread senderThread = new SenderThread(settings.getSenderThreadInterval(), resultsBlockingQueue);

		// register shutdown hook
		Runtime.getRuntime().addShutdownHook(new ShutdownThread(this, periodicThreadList, executorService, senderThread));

		// job provider
		IJobProvider jobProvider = null;

		if (settings.getGenerateJobs())
			jobProvider = new JobGenerator(settings.getJobFileName(), settings.getMaxNextJobs(), resultsBlockingQueue);
		else
			jobProvider = new JobReader(settings.getJobFileName(), settings.getMaxNextJobs(), resultsBlockingQueue);

		// Get Job requests from AM. In part1 we read from a file.
		while (jobProvider.hasMoreJobs() && (!this.isInterrupted())) {

			Job[] jobsBatch = jobProvider.getNextJobs();

			log.info(Thread.currentThread().getName() + ": Received " + jobsBatch.length + " jobs.");

			for (Job job : jobsBatch) {

				if (this.isInterrupted())
					break;

				if (job.isPeriodic()) {
					Thread pt = new PeriodicThread(job.getId(), job, job.getPeriod());
					periodicThreadList.add(pt);

				} else {
					executorService.submit(job);
				}
			}

			if (this.isInterrupted())
				break;

			try {
				Thread.sleep(settings.getJobRequestInterval() * 1000);

			} catch (InterruptedException e) {

				log.info(this + ": Interrupted while sleeping.");

				this.interrupt();
			}
		}

		System.out.println();

		if (!this.isInterrupted())
			System.out.println(this + ": All jobs submitted.");
		else
			System.out.println(this + ": Application stopping before all jobs were received.");

	}
}
