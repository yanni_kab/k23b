package k23b.sa.Threads;

import java.util.ArrayList;
import java.util.List;

import k23b.sa.Result;
import k23b.sa.BlockingQueue.IBlockingQueue;

import org.apache.log4j.Logger;

/**
 * Implements the functionality of sending the results that are stored in the blocking queue to the Aggregator manager (on part1 it outputs to stdout)
 *
 */
public class SenderThread extends Thread {

	private static final Logger log = Logger.getLogger(SenderThread.class);

	private int period;

	private IBlockingQueue<Result> jobResultsBlockingQueue;

	/**
	 * @param period the time interval between result sending. It uses the senderThreadInterval property value
	 * @param jobResultsBlockingQueue the shared queue between WorkerThread and PeriodicThred for sending the job results to the Aggregator Manager
	 */
	public SenderThread(int period, IBlockingQueue<Result> jobResultsBlockingQueue) {

		this.period = period;

		this.jobResultsBlockingQueue = jobResultsBlockingQueue;

		setName("Sender");

		log.debug(this + ": Starting.");

		start();
	}

	@Override
	public String toString() {
		return getName();
	}

	/**
	 * Invokes the {@link #sendResults() sendResults} every period secs
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {

		log.info(this + ": Running.");

		// Check if it is interrupted after a sleep or before even starting
		while (!isInterrupted()) {

			sendResults();

			try {
				// If thread has been interrupted, sleep() will immediately throw an InterruptedException
				Thread.sleep(period * 1000);

			} catch (InterruptedException e) {
				// Got an interrupt()
				log.info(this + ": Interrupted while sleeping.");

				// set the interrupt flag again so as to break out of loop
				interrupt();
			}
		}

		Thread.interrupted(); // this will clear the interrupted flag so we can get results with queue.get()
		sendResults(); // make one last pass since we were interrupted and the results queue may still have some items

		String message = this + ": Finished.";
		log.info(message);
		System.out.println(System.lineSeparator() + message);
	}

	/**
	 * Sends the results that are stored in the blocking queue to the Aggregator manager
	 */
	private void sendResults()
	{
		// Send the current size of the jobResults Queue

		int resultsSize = jobResultsBlockingQueue.size();

		String message = this + ": Results queue size is " + resultsSize + ".";
		System.out.println(System.lineSeparator() + message);
		log.info(message);

		if (resultsSize > 0) {

			List<Result> results = new ArrayList<Result>(resultsSize);

			while (jobResultsBlockingQueue.size() > 0)

				try {

					Result r = jobResultsBlockingQueue.get();

					results.add(r);

				} catch (InterruptedException e) {

					log.error(this + ": InterruptedException while getting result from queue.");

					interrupt(); // because the interrupted has been cleared on exception handling

					break;
				}

			// send the results to AM
			for (Result r : results) {
				System.out.println(r);
			}
		}
	}
}
