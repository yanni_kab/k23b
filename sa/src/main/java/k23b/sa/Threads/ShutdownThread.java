package k23b.sa.Threads;

import java.util.List;

import k23b.sa.ExecutorService.IExecutorService;

import org.apache.log4j.Logger;

/**
 * Initializes the addShutdownHook call of Runtime. Provides the smooth shutting down of the application
 *
 */
public class ShutdownThread extends Thread {

	private static final Logger log = Logger.getLogger(ShutdownThread.class);

	private Thread mainThread;
	private List<Thread> periodicThreadList;
	private Thread senderThread;
	private IExecutorService workerThreadPool;

	/**
	 * @param mainThread the thread of the App
	 * @param periodicThreadList a list of the started PeriodicThreads
	 * @param executorService the ThreadPool
	 * @param senderThread the SenderThread
	 */
	public ShutdownThread(Thread mainThread, List<Thread> periodicThreadList, IExecutorService executorService, Thread senderThread) {
		this.mainThread = mainThread;
		this.periodicThreadList = periodicThreadList;
		this.senderThread = senderThread;
		this.workerThreadPool = executorService;

		setName("Shutdown");
	}

	@Override
	public String toString() {
		return getName();
	}

	/**
	 * Interrupting the PeriodicThreads, ThreadPool and SenderThread and waiting for them to end in the aforementioned order
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {

		log.info(this + ": Control-C caught. Shutting down...");
		System.out.print(System.lineSeparator() + this + ": Control-C caught. Shutting down Software Agent." + System.lineSeparator());

		// main thread termination

		mainThread.interrupt();

		try {
			mainThread.join();

		} catch (InterruptedException e) {
			log.error(this + ": InterruptedException on main thread join().");
		}

		// periodic threads termination

		for (Thread t : periodicThreadList) {
			t.interrupt();
		}

		for (Thread t : periodicThreadList) {
			try {
				t.join();

			} catch (InterruptedException e) {
				log.error(this + ": InterruptedException during periodic thread join().");
			}
		}

		System.out.println(System.lineSeparator() + this + ": " + periodicThreadList.size() + " periodic threads terminated.");

		// threadpool termination

		workerThreadPool.shutdown();

		try {
			workerThreadPool.awaitTermination();

			System.out.println(System.lineSeparator() + this + ": Thread pool terminated.");

		} catch (InterruptedException e) {
			log.error(this + ": InterruptedException during executor service awaitTermination().");
		}

		// sender thread interruption

		senderThread.interrupt();

		try {
			senderThread.join();

		} catch (InterruptedException e) {
			log.error(this + ": InterruptedException during sender thread join().");
		}
	}
}
