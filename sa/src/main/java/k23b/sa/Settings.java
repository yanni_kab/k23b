package k23b.sa;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Get/Set the configurable parameters of the application
 *
 */
public class Settings {

	private static final Logger log = Logger.getLogger(Settings.class);

	private String jobFileName;

	public String getJobFileName() {

		return this.jobFileName;
	}

	private void setJobFileName(String jobFileName) {

		this.jobFileName = jobFileName;
	}

	private boolean generateJobs;

	public boolean getGenerateJobs() {
		return this.generateJobs;
	}

	private void setGenerateJobs(boolean generateJobs) {
		this.generateJobs = generateJobs;
	}

	private int threadPoolSize;

	public int getThreadPoolSize() {

		return this.threadPoolSize;
	}

	private void setThreadPoolSize(int threadPoolSize) {

		if (threadPoolSize <= 0)
			threadPoolSize = 1;

		this.threadPoolSize = threadPoolSize;
	}

	private int maxNextJobs;

	public int getMaxNextJobs() {

		return this.maxNextJobs;
	}

	private void setMaxNextJobs(int maxNextJobs) {

		if (maxNextJobs <= 0)
			maxNextJobs = 10;

		this.maxNextJobs = maxNextJobs;
	}

	private int senderThreadInterval;

	public int getSenderThreadInterval() {

		return this.senderThreadInterval;
	}

	private void setSenderThreadInterval(int senderThreadInterval) {

		if (senderThreadInterval <= 0)
			senderThreadInterval = 60;

		this.senderThreadInterval = senderThreadInterval;
	}

	private boolean runNmapAsRoot;

	public boolean getRunNmapAsRoot() {
		return this.runNmapAsRoot;
	}

	private void setRunNmapAsRoot(boolean runNmapAsRoot) {
		this.runNmapAsRoot = runNmapAsRoot;
	}

	private int jobRequestInterval;

	public int getJobRequestInterval() {

		return this.jobRequestInterval;
	}

	private void setJobRequestInterval(int jobRequestInterval) {

		if (jobRequestInterval <= 0)
			jobRequestInterval = 60;

		this.jobRequestInterval = jobRequestInterval;
	}

	private int resultsQueueMax;

	public int getResultsQueueMax() {
		return this.resultsQueueMax;
	}

	private void setResultsQueueMax(int resultsQueueMax) {
		this.resultsQueueMax = resultsQueueMax;
	}

	private String propertyFileName;

	public Settings(String propertyFileName) {

		this.propertyFileName = propertyFileName;
	}

	public void load()
	{
		Properties properties = new Properties();

		File propertyFile = new File(this.propertyFileName);

		FileReader fr = null;

		try {

			fr = new FileReader(propertyFile);

			try {
				log.info("Loading settings.");

				properties.load(fr);

				setJobFileName(properties.getProperty("jobFileName"));
				log.info("jobFileName: " + getJobFileName());

				setGenerateJobs(properties.getProperty("generateJobs").equals("true"));
				log.info("generateJobs: " + getGenerateJobs());

				setJobRequestInterval(Integer.parseInt(properties.getProperty("jobRequestInterval")));
				log.info("jobRequestInterval: " + getJobRequestInterval());

				setMaxNextJobs(Integer.parseInt(properties.getProperty("maxNextJobs")));
				log.info("maxNextJobs: " + getMaxNextJobs());

				setThreadPoolSize(Integer.parseInt(properties.getProperty("threadPoolSize")));
				log.info("threadPoolSize: " + getThreadPoolSize());
				
				setRunNmapAsRoot(properties.getProperty("runNmapAsRoot").equals("true"));
				log.info("runNmapAsRoot: " + getRunNmapAsRoot());

				setResultsQueueMax(Integer.parseInt(properties.getProperty("resultsQueueMax")));
				log.info("resultsQueueMax: " + getResultsQueueMax());

				setSenderThreadInterval(Integer.parseInt(properties.getProperty("senderThreadInterval")));
				log.info("senderThreadInterval: " + getSenderThreadInterval());
				
				log.info("Settings loaded.");

			} catch (IOException e) {
				log.error("Error while loading settings: " + e.toString());
			}

		} catch (FileNotFoundException e) {
			log.error("Property file not found: " + propertyFile.toString());
		} finally {
			try {

				log.info("Closing settings file.");

				if (fr != null)
					fr.close();

			} catch (IOException e) {
				log.error("Error while closing settings file: " + propertyFile.toString());
			}
		}
	}
}
