package k23b.sa;

import k23b.sa.Threads.MainThread;

/**
 * The application for the Software Agent
 *
 */
public class App {

	public static void main(String[] args) {

		new MainThread().start();
	}
}
