package k23b.sa;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;


/**
 * It holds the result as well as the Job id from a nmap task.
 *
 */
public class Result {

	private int jobId;
	private String jobResult;

	public Result(int jobId, String jobResult) {
		this.jobId = jobId;
		this.jobResult = jobResult;
	}

	@Override
	public String toString() {

		String s = String.format("[%d] ", this.jobId);

		String l1 = s;
		String l2 = "";

		if (jobResult != null) {

			StringTokenizer st = new StringTokenizer(jobResult, System.lineSeparator());

			try {

				for (int i = 0; i < 4; i++)
					st.nextToken();

				l1 = l1 + st.nextToken();

				StringBuilder sb = new StringBuilder();

				sb.append(System.lineSeparator());

				for (int j = 0; j < s.length(); j++)
					sb.append(" ");

				sb.append(st.nextToken());

				l2 = sb.toString();

				try {
					l1 = l1.substring(0, 80);
				} catch (IndexOutOfBoundsException e) {
				}

				try {
					l2 = l2.substring(0, 80);
				} catch (IndexOutOfBoundsException e) {
				}

			} catch (NoSuchElementException e) {
			}
		}

		return l1 + l2;
	}
}
